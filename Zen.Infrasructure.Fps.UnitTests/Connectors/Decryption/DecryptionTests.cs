﻿using System;
using Zen.Infrasructure.Fps;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Configuration;
using Zen.Infrastructure.Fps.Connectors.Decryption;
using Zen.Infrastructure.Logging;
using Zen.Infrastructure.Fps.Interface.Decryption;

namespace Zen.Infrasructure.Fps.UnitTests.Connectors.Decryption.DecryptionTests
{
    [TestClass]
    public class DecryptionTests
    {
        private ILogger logger;
        private IDecryptConnectorResponse decryptConnectorResponse;
        private string encryptedFiles = string.Empty;
        private string privateKeyPath = string.Empty;
        private string decryptPassword = string.Empty;
        private string decryptOutputPath = string.Empty;
        private string publicKeyPath = string.Empty;
        private string destination = string.Empty;

        [TestInitialize]
        public void Initialize()
        {
            encryptedFiles = @"D:\FTPEncryptedFiles\";
            privateKeyPath = @"D:\ZnalyticsKeys\keyPrivate.txt";
            decryptPassword = "rama";
            decryptOutputPath = @"D:\FTPDecryptedFiles";
            publicKeyPath = @"D:\ZnalyticsKeys\keyPublic.txt";
            destination = @"D:\FTPEncryptedFiles\";

            decryptConnectorResponse = new DecryptConnectorResponse();
            logger = new SeriLogLogger("http://logs.znalytics.com:9200", "DecryptTest");
            
        }

        [TestMethod]
        public void DecryptFile_AllValidData_Test()
        {
            DecryptDestinationConnector decryptdestinationConnector = new DecryptDestinationConnector();

            decryptConnectorResponse = decryptdestinationConnector.DecryptFile(encryptedFiles, privateKeyPath, decryptPassword, decryptOutputPath, publicKeyPath, logger);

            Assert.IsTrue(decryptConnectorResponse.Success);
        }

        [TestMethod]
        public void DecryptFile_EncryptedFilePath_Empty_Test()
        {
            DecryptDestinationConnector decryptdestinationConnector = new DecryptDestinationConnector();

            decryptConnectorResponse = decryptdestinationConnector.DecryptFile(string.Empty, privateKeyPath, decryptPassword, decryptOutputPath, publicKeyPath, logger);

            Assert.IsFalse(decryptConnectorResponse.Success);
        }

        [TestMethod]
        public void DecryptFile_PrivateFilePath_Empty_Test()
        {
            DecryptDestinationConnector decryptdestinationConnector = new DecryptDestinationConnector();

            decryptConnectorResponse = decryptdestinationConnector.DecryptFile(encryptedFiles, string.Empty, decryptPassword, decryptOutputPath, publicKeyPath, logger);

            Assert.IsFalse(decryptConnectorResponse.Success);
        }

        [TestMethod]
        public void DecryptFile_DecryptPassword_Empty_Test()
        {
            DecryptDestinationConnector decryptdestinationConnector = new DecryptDestinationConnector();

            decryptConnectorResponse = decryptdestinationConnector.DecryptFile(encryptedFiles, privateKeyPath, string.Empty, decryptOutputPath, publicKeyPath, logger);

            Assert.IsFalse(decryptConnectorResponse.Success);
        }

        [TestMethod]
        public void DecryptFile_DecryptOutputPath_Empty_Test()
        {
            DecryptDestinationConnector decryptdestinationConnector = new DecryptDestinationConnector();

            decryptConnectorResponse = decryptdestinationConnector.DecryptFile(encryptedFiles, privateKeyPath, decryptPassword, string.Empty, publicKeyPath, logger);

            Assert.IsFalse(decryptConnectorResponse.Success);
        }

        [TestMethod]
        public void DecryptFile_PublicKeyPath_Empty_Test()
        {
            DecryptDestinationConnector decryptdestinationConnector = new DecryptDestinationConnector();

            decryptConnectorResponse = decryptdestinationConnector.DecryptFile(encryptedFiles, privateKeyPath, decryptPassword, decryptOutputPath, string.Empty, logger);

            Assert.IsFalse(decryptConnectorResponse.Success);
        }
    }
}
